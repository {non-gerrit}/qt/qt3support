TEMPLATE = subdirs

module_qt3support_src.subdir = src
module_qt3support_src.target = module-qt3support-src

module_qt3support_tools.subdir = tools
module_qt3support_tools.target = module-qt3support-tool
module_qt3support_tools.depends = module_qt3support_src

module_qt3support_examples.subdir = examples
module_qt3support_examples.target = module-qt3support-examples
module_qt3support_examples.depends = module_qt3support_src

module_qt3support_tests.subdir = tests
module_qt3support_tests.target = module-qt3support-test
module_qt3support_tests.depends = module_qt3support_src
module_qt3support_tests.CONFIG = no_default_target no_default_install

SUBDIRS += module_qt3support_src \
           module_qt3support_tools \
           module_qt3support_examples \
           module_qt3support_tests \
