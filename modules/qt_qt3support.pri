QT_QT3SUPPORT_VERSION = $$QT_VERSION
QT_QT3SUPPORT_MAJOR_VERSION = $$QT_MAJOR_VERSION
QT_QT3SUPPORT_MINOR_VERSION = $$QT_MINOR_VERSION
QT_QT3SUPPORT_PATCH_VERSION = $$QT_PATCH_VERSION

QT.qt3support.name = Qt3Support
QT.qt3support.bins = $$QT_MODULE_BIN_BASE
QT.qt3support.includes = $$QT_MODULE_INCLUDE_BASE $$QT_MODULE_INCLUDE_BASE/Qt3Support
QT.qt3support.private_includes = $$QT_MODULE_INCLUDE_BASE/Qt3Support/private
QT.qt3support.sources = $$QT_MODULE_BASE/src/qt3support
QT.qt3support.libs = $$QT_MODULE_LIB_BASE
QT.qt.plugins = $$QT_MODULE_PLUGIN_BASE
QT.qt3support.imports = $$QT_MODULE_IMPORT_BASE
QT.qt3support.depends = core gui
QT.qt3support.DEFINES = QT3_SUPPORT QT_QT3SUPPORT_LIB

contains(QT_CONFIG, gui-qt3support) {
    QT_CONFIG += qt3support
} else {
    warning("Attempted to include $$QT.qt3support.name in the build, but $$QT.gui.name was not built with $$QT.qt3support.name enabled.")
}
